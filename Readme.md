A few lines of code to download youtube videos.

More info about pytube:

https://pypi.org/project/pytube/

Pendiente:

- Que al procesar un archivo de links te diga
  cuantos links hay, y que te muestre en cada
  momento cual de ellos está procesado pero 
  si es posible el título del video en vez 
  de la direccion url pero ok en el peor de 
  los casos.
  
- También debes cambiar el interprete a un 
  docker porque esta librería te da problemas
  con el python 3.6 al incluir el módulo 
  collection en su versión 3.8

Incidencias:

- The playlists must be public
- Tio me daba un error con la API pensé que era cosa de la versión 
  del pytube pero no, porque al día siguiente me lo volvió a hacer, 
  el rollo es que le di a run otra vez y a la segunda lo hizo bien, 
  es como que a veces no le responde el servidor y hay que volver 
  a intentarlo una y otra vez.

  Or it is downloading files, but then suddenly it can't download anymore
  any of them... and give that error of connection on the 'request' library.

  To avoid it try to download the files when the server is not very loaded.  

  Try also to connect with another IP.

- I was a couple of hours trying it works and I was getting crazy.
  The command line commands worked perfectly but something was wrong
  with the YouTube or PlayList object, error 404. So the final solution
  was just to unistall pytube and install the last version.
  
  How I did so? easy, I went to Python Packages in the down bar here 
  in Webstorm and I delete pytube, then in requirements I typed the last
  pytube version available in pypi.org and then webstorm asked me to 
  install that requirement, I clicked in ok, and then everything 
  worker perfectly.
