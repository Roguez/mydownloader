# importing the module
import inspect

from pytube import YouTube, Playlist
import time

def exception_details(inst):
    print(type(inst))  # the exception instance
    print(type(inst))
    print(inst.args)
    print(inst)


def d_only_audio_pl(link, SAVE_PATH):
    pl = Playlist(link)
    print(f'Downloading: {pl.title}')
    for video in pl.videos:
        t = video.streams.filter(only_audio=True)
        t[0].download(SAVE_PATH)

def d_only_audio_sv(link, SAVE_PATH):
    yt = YouTube(link)
    # print('hiiiihiii')
    # print(f'Downloading: {yt.title}')
    # print('opppsss')
    try:
        t = yt.streams.filter(only_audio=True)
    except Exception as inst:
        print("An exception has occurred in d_only_audio_sv")
        exception_details(inst)
    else:
        t[0].download(SAVE_PATH)

def d_video_audio_sv(link, SAVE_PATH):
    try:
        yt = YouTube(link)
        # 1 print(yt.streams.filter(file_extension='mp4', progressive=True))
        # 1 itag = str(input("Choise a tag:"))
        # La idea es seleccionar una itag que tenga el video y el audio.
        # Al haber filtrado por "progressive" lo normal es que todas tengan audio y video,
        # Pero la máxima resolución que puedes entontrar es un 720
        # Si quieres resoluciones mayores tienes que bajarte el audio y el video separados
        # y luego unirlos con FFmpeg por ejemplo.

        # 1 d_video = yt.streams.get_by_itag(itag)

        # order_by('resolution').desc().first()

        # , resolution='360p')[0]
        # the lower index, the higher quality.

        # could use .first() or .one() instead of [index], the lower the index, the higher quality.

        # .one() requires that there is only one result in the result set; it is an error
        # if the database returns 0 or 2 or more results and an exception will be raised.

        # returns the first of a potentially larger result set (adding LIMIT 1 to the query),
        # or None if there were no results. No exception will be raised.
        try:
            # downloading the video
            # 1 d_video.download(SAVE_PATH)
            yt.streams.filter(progressive=True, file_extension='mp4').order_by(
                'resolution').desc().first().download(SAVE_PATH)
        except Exception as inst:
            print("Could connect but can't download")
            exception_details(inst)

            cant_download = True
    except Exception as inst:
        print("Connection Error")
        exception_details(inst)
        wrong_connection = True

def d_video_audio_pl(link, SAVE_PATH):
    pl = Playlist(link)
    print(f'Downloading: {pl.title}')
    for video in pl.videos:
#        try:
#            yt = YouTube(link)
            try:
                t = video.streams.filter(progressive=True, file_extension='mp4').order_by(
                    'resolution').desc()
                t[0].download(SAVE_PATH)
            except Exception as inst:
                print("Could connect but can't download")
                exception_details(inst)
                cant_download = True
 #       except:
 #           print("Connection Error")

def downloader():
    # where to save
    SAVE_PATH = "/media/fer/70FED19202F10E8E1/Russian_Max/Beginner"
    # to_do
    wrong_option = False
    wrong_connection = False
    cant_download = False

    # -------- Aquí se pueden poner URL individuales -----------------------------------------------

    # link = 'https://www.youtube.com/watch?v=2ohbjep_wjq'
    # yt = youtube(link)
    # d_video = yt.streams.filter(file_extension='mp4', resolution='720p').first()
    # d_video.download(save_path)

    # ----------------------------------------------------------------------------------------------
    # link of the video to be downloaded
    links = open('links_file.txt', 'r')  # opening the file

    option = str(input("(1) Only audio, (2) Audio and Video. Choice an option: "))
    option2 = str(input("(y/n) Are the links playlist's links?"))

    if option == "1":

        for link in links:
            print(f"\t {link}")

            if option2 == "y":
                d_only_audio_pl(link, SAVE_PATH)
            else:
                d_only_audio_sv(link,SAVE_PATH)

        print('Task Completed!')

    elif option == "2":

        for link in links:
            print(f"\t {link}")
            if option2 =="y":
                d_video_audio_pl(link, SAVE_PATH)
            else:
                d_video_audio_sv(link, SAVE_PATH)

        print('Task Completed!')

    else:
        print("Wrong option, choice again.")
        wrong_option = True

    if not (wrong_option or wrong_connection or cant_download):
        print('Task Completed!')

if __name__ == "__main__":
    downloader()
